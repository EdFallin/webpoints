
function pageService() {
// region external dependencies 

    const app = require("./app");
    const debug = require("debug")("webpoints:server");
    const http = require("http");

// endregion external dependencies 

// region set up for, create, and start page service / server

    var port = normalizePort(process.env.PORT || "3000");
    app.set("port", port);

    var server = http.createServer(app);

    server.on("error", onError);
    server.on("listening", onListening);

    /* actually start the page service / server */
    server.listen(port);

// endregion set up for, create, and start page service / server

// region internal dependencies, including eventing

    function normalizePort(val) {
        var port = parseInt(val, 10);

        if (isNaN(port)) {
            // named pipe
            return val;
        }

        if (port >= 0) {
            // port number
            return port;
        }

        return false;
    }

    function onError(error) {
        if (error.syscall !== "listen") {
            throw error;
        }

        var bind = typeof port === "string"
            ? "Pipe " + port
            : "Port " + port;

        // handle specific listen errors with friendly messages
        switch (error.code) {
            case "EACCES":
                console.error(bind + " requires elevated privileges");
                process.exit(1);
                break;
            case "EADDRINUSE":
                console.error(bind + " is already in use");
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    function onListening() {
        var addr = server.address();
        var bind = typeof addr === "string"
            ? "pipe " + addr
            : "port " + addr.port;
        debug("Listening on " + bind);
    }

// endregion internal dependencies, including eventing
}

module.exports = pageService;
