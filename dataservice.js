/* definition and start-up of data service, written in
   require() style to match the rest of NodeJS Express */

function dataService() {
// region external dependencies

    var express = require("express");
    var parser = require("body-parser");

// endregion external dependencies

// region service instance and wiring its components

    var service = express();

    // apply URL and JSON parsing using the external dependency
    service.use(parser.urlencoded({extended: true}));
    service.use(parser.json());

    // wrap all incoming HTTP requests so any from anywhere are allowed (with "*")
    service.use((request, response, next) => {
        response.header("Access-Control-Allow-Origin", "*");
        response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

// endregion service instance and wiring its components

// region get(), post(), and other HTTP responders

    service.get("/", (request, response) => {
        response.send("Service responded.");
    });

    service.get("/plain/data", (request, response) => {
        response.send("Here is the plain data you requested.");
    });

    service.get("/based/on/:input", (request, response) => {
        let input = request.params.input;

        // turning text to an array and back,
        // to allow reversing (in place)
        let array = Array.from(input);
        array.reverse();
        let reversed = array.join("");

        let output = {forwards: input, backwards: reversed};
        response.send(output);
    });

// endregion get(), post(), and other HTTP responders

// region starting the service

    service.listen(4000, () => {
        console.log("Data service has started listening on port 4000.");
    });

// endregion starting the service
}

/* this exports line defines what require() elsewhere will retrieve */
module.exports = dataService;
