// region definitions

export class IndexController {
    constructor(dom) {
        this.dom = dom;
    }

    run() {
        this.wire();
    }

    wire() {
        // region DOM identities

        // buttons
        this.getPredefined = this.dom.getElementById("GetPredefinedData");
        this.getBasedOnInput = this.dom.getElementById("GetDataBasedOnInput");

        // inputs
        this.input = this.dom.getElementById("TextInput");

        // outputs from data service
        this.predefined = this.dom.getElementById("PredefinedResult");
        this.basedOnInput = this.dom.getElementById("BasedOnInputResult");

        // endregion DOM identities

        // region eventing

        this.getPredefined.addEventListener("click", (e) => {
            // deliberately ignoring returned promise
            this.getDataNoArgs(e);
        });
        this.getBasedOnInput.addEventListener("click", (e) => {
            // deliberately ignoring returned promise
            this.getDataWithArgs(e);
        });

        // endregion eventing
    }

    // async just to allow await to be used, at least in this case
    async getDataNoArgs(e) {
        // in this context, 'this' is the IndexController instance

        this.predefined.classList.remove("placeholder");
        this.predefined.classList.add("waiting");

        try {
            // get data from the service using awaiting
            let result = await fetch("http://localhost:4000/plain/data");

            // set data on the page in an implicit continuation,
            // or set a fail warning if the fetch failed
            if (result.ok) {
                // get the text form of the returned data using awaiting
                let text = await result.text();

                // apply the text from the data in an implicit continuation
                this.styleAsRetrieved(this.predefined, text);
            }
            else {
                this.styleAsFailed(this.predefined);
            }
        }
        catch (e) {
            this.styleAsFailed(this.predefined);
        }
    }

    async getDataWithArgs(e) {
        // in this context, 'this' is the IndexController instance

        try {
            let input = this.input.value;
            let response = await fetch("http://localhost:4000/based/on/" + input);

            if (response.ok) {
                let json = await response.json();

                let backwards = json.backwards;
                let forwards = json.forwards;

                let text = `"${forwards}" was reversed to "${backwards}".`;
                this.styleAsRetrieved(this.basedOnInput, text);
            }
            else {
                throw Error("Fetch failed.");
            }
        }
        catch {
            this.styleAsFailed(this.basedOnInput);
        }

    }

    styleAsRetrieved(el, text) {
        // any possible past state classes are removed
        el.classList.remove("placeholder", "waiting", "failed");

        // state class and text are set
        el.classList.add("retrieved");
        el.innerHTML = text;
    }

    styleAsFailed(el) {
        // any possible past state classes are removed
        el.classList.remove("placeholder", "waiting", "retrieved");

        // state class and text are set
        el.classList.add("failed");
        el.innerHTML = "Data fetch failed.";
    }
}

// region definitions

// region runtime

// creating my instance and starting its operations
let controller = new IndexController(document);
controller.run();

// endregion runtime
